package za.sargamatha.iol.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import za.sargamatha.iol.models.Article
import za.sargamatha.iol.models.ArticleConcept
import za.sargamatha.iol.network.ApiClient
import za.sargamatha.iol.network.ApiInterface
import za.sargamatha.iol.utils.Constants


class ArticleConceptRepository {

    private var apiInterface: ApiInterface? = null

    init{
        apiInterface = ApiClient.getApiClient().create(ApiInterface::class.java)
    }
    companion object{
        private var TAG = ArticleConceptRepository::class.java.simpleName
    }
    private val data = MutableLiveData<ArticleConcept>()


    fun fetchArticleUuuids(): MutableLiveData<ArticleConcept> {
        apiInterface?.fetchAllArticleUuids(
            Constants.DATE_RANGE, Constants.CHANNEL,Constants.CONTENT_TYPE,
            Constants.SORT_INDEX, Constants.SORT_VALUE, Constants.GET_UUID_PROPERTIES,
            Constants.LIMIT
        )?.enqueue(object :
            Callback<ArticleConcept> {
            override fun onResponse(
                call: Call<ArticleConcept>,
                response: Response<ArticleConcept>
            ) {

                val res = response.body()
                if (response.code() == 200 && res != null) {
                    data.value = res

                } else {
                    data.value = null
                }
            }

            override fun onFailure(call: Call<ArticleConcept>, t: Throwable) {
                data.value = null
                Log.d(TAG, "onFailure: $t")
            }

        })

        return data
    }

    fun fetchArticles(hits: String): MutableLiveData<Article>{
        val data = MutableLiveData<Article>()

        apiInterface?.fetchAllArticles(hits, Constants.GET_ARTICLE_PROPERTIES)?.enqueue(
            object : Callback<Article> {
                override fun onResponse(
                    call: Call<Article>,
                    response: Response<Article>
                ) {

                    val res = response.body()
                    if (response.code() == 200 && res != null) {
                        data.value = res

                    } else {
                        data.value = null
                    }
                }

                override fun onFailure(call: Call<Article>, t: Throwable) {
                    data.value = null
                    Log.d(TAG, "onFailure: FetchAllArticles $t")
                }

        })

        return data
    }

//    fun fetchAllArticlesSection(){
//        apiInterface?.fetchAllArticlesSection().
//    }

    fun fetchFeaturedList(): MutableLiveData<String>{
        val data = MutableLiveData<String>()
        apiInterface?.fetchFeaturedList(Constants.FEATURED_LIST_UUID)?.enqueue(
            object : Callback<String> {
                override fun onResponse(
                    call: Call<String>,
                    response: Response<String>
                ) {

                    val res = response.body()
                    if (response.code() == 200 && res != null) {
                        data.value = res

                    } else {
                        data.value = null
                    }
                    Log.d(TAG, "onResponse: $res")
                }

                override fun onFailure(call: Call<String>, t: Throwable) {
                    data.value = null
                    Log.d(TAG, "onFailure: FetchFeaturedList $t")
                }
            })


        return data
    }



}