package za.sargamatha.iol.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import za.sargamatha.iol.R
import za.sargamatha.iol.models.Concept

class FollowConceptAdapter: RecyclerView.Adapter<FollowConceptAdapter.ViewHolder>() {

    private lateinit var conceptArrayList: ArrayList<Concept>

    fun setData(conceptArrayList: ArrayList<Concept>){
        this.conceptArrayList = conceptArrayList
    }
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
     val view = LayoutInflater.from(parent.context).inflate(R.layout.follow_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       // Set concept items
        holder.followBtn.setOnClickListener {

        }
    }

    override fun getItemCount(): Int {
        return 3 //conceptArrayList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val conceptTitle: TextView = itemView.findViewById(R.id.conceptTitle)
        val conceptItemTitle: TextView = itemView.findViewById(R.id.conceptItemTitle)
        val followBtn: TextView = itemView.findViewById(R.id.follow_concept_btn)

    }
}