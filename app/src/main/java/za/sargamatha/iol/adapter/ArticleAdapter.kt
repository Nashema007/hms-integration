package za.sargamatha.iol.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat.getColor
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import za.sargamatha.iol.R
import za.sargamatha.iol.models.Article
import za.sargamatha.iol.utils.Constants
import za.sargamatha.iol.utils.DateFormatter
import za.sargamatha.iol.views.fragments.FeaturedFragmentDirections
import za.sargamatha.iol.views.fragments.LatestNewsFragmentDirections

class ArticleAdapter : RecyclerView.Adapter<ArticleAdapter.ViewHolder>() {

    private var articleArray: ArrayList<Article>? = null
    private lateinit var classFrom: String

    companion object{
        private var TAG = ArticleAdapter::class.java.simpleName
    }

    fun setData(articleArray: ArrayList<Article>, classFrom: String) {
        this.articleArray = articleArray
        this.classFrom = classFrom
        notifyDataSetChanged()
    }
    fun clear(){
        this.articleArray!!.clear()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.article_item, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // Implement article data
        val data = articleArray!![position]
        try {

            val section = data.properties[1].values[0]
            holder.articleAuthor.text = if(data.properties[0].values[0].trim() != null) data.properties[0].values[0].trim()  else " "
            holder.articleTitle.text = data.properties[4].values[0].trim()
            holder.articleTeaser.text = if(data.properties[2].values[0].trim() != null) data.properties[2].values[0].trim() else " "
            holder.articleDate.text = DateFormatter().format(data.properties[11].values[0].trim())
            val imgUrl = Constants.IMAGE_API+data.properties[7].values[0]+ Constants.IMAGE_API_PROPERTIES
            Picasso.get().load(imgUrl).into(holder.articleImage)

            holder.articleSection.text = section
            when(section){
                "Lifestyle"-> holder.articleSection.setBackgroundColor(getColor(holder.itemView.context,R.color.Lifestyle))
                "Industry" -> holder.articleSection.setBackgroundColor(getColor(holder.itemView.context,R.color.Industry))
                "Sport" -> holder.articleSection.setBackgroundColor(getColor(holder.itemView.context,R.color.Sport))
                "Partnered" -> holder.articleSection.setBackgroundColor(getColor(holder.itemView.context,R.color.Partnered))
                "Africa" -> holder.articleSection.setBackgroundColor(getColor(holder.itemView.context,R.color.Africa))
                "Technology" -> holder.articleSection.setBackgroundColor(getColor(holder.itemView.context,R.color.Technology))
                "News" -> holder.articleSection.setBackgroundColor(getColor(holder.itemView.context, R.color.red))
            }
            holder.articleLayout.setOnClickListener {
                if (classFrom == "Featured") {
                    val action =
                        FeaturedFragmentDirections.actionFeaturedFragmentToArticleFragment(data)
                    holder.itemView.findNavController().navigate(action)
                } else if (classFrom == "LatestNews") {
                    val action = LatestNewsFragmentDirections.actionLatestNewsFragmentToArticleFragment(data)
                    holder.itemView.findNavController().navigate(action)
                }
            }

        }
        catch (e:Exception){
            Log.d(TAG, "onBindViewHolder: Position: $position -> $e")
        }


    }


    override fun getItemCount(): Int {
        return if (articleArray != null) articleArray!!.size else 0
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val articleTitle: TextView = itemView.findViewById(R.id.articleTitle)
        val articleTeaser: TextView = itemView.findViewById(R.id.article_teaser)
        val articleImage: ImageView = itemView.findViewById(R.id.articleImageView)
        val articleAuthor: TextView = itemView.findViewById(R.id.article_author)
        val articleDate: TextView = itemView.findViewById(R.id.article_date_time)
        val articleLayout: CardView = itemView.findViewById(R.id.articleCardView)
        val articleSection: TextView = itemView.findViewById(R.id.articleSection)

    }
}