package za.sargamatha.iol.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import za.sargamatha.iol.R

class EditSectionAdapter: RecyclerView.Adapter<EditSectionAdapter.ViewHolder>() {

    private var listItems = ArrayList<String>()
    fun setData( listItems: ArrayList<String>){
        this.listItems = listItems
    }
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
       val view = LayoutInflater.from(parent.context).inflate(R.layout.edit_choice_item, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val section = listItems[position]
        holder.section.text = section
    }

    override fun getItemCount(): Int {
        return listItems.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val section: TextView = itemView.findViewById(R.id.editSection)

    }


}