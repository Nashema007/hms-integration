package za.sargamatha.iol.views.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import za.sargamatha.iol.R


class AddSectionsFragment : Fragment() {

    private lateinit var save: Button
    private lateinit var cancel: Button
    private lateinit var addSectionRecyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_add_sections, container, false)

        save = view.findViewById(R.id.saveSection)
        cancel = view.findViewById(R.id.cancelSection)
        addSectionRecyclerView = view.findViewById(R.id.addSectionRecyclerView)

        save.setOnClickListener {
            val action = AddSectionsFragmentDirections.actionAddSectionsFragmentToMyNewsFragment()
            findNavController().navigate(action)
        }

        cancel.setOnClickListener {
            val action = AddSectionsFragmentDirections.actionAddSectionsFragmentToMyNewsFragment()
            findNavController().navigate(action)
        }


        return view
    }


}