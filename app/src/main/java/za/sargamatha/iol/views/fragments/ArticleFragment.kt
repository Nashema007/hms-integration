package za.sargamatha.iol.views.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.SimpleExoPlayer
import com.squareup.picasso.Picasso
import za.sargamatha.iol.R
import za.sargamatha.iol.models.Article
import za.sargamatha.iol.utils.ArticleBuilder
import za.sargamatha.iol.utils.Constants
import za.sargamatha.iol.utils.DateFormatter
import za.sargamatha.iol.utils.XmlParser
import java.io.StringReader


class ArticleFragment : Fragment() {

    private val args: ArticleFragmentArgs by navArgs()
    private lateinit var articleImage: ImageView
    private lateinit var articleHeadline: TextView
    private lateinit var articleTeaserBody: TextView
    private lateinit var articlePub: TextView
    private lateinit var articleSection: TextView
    private lateinit var articleBody: TextView
    private lateinit var articleBodySub: TextView
    private lateinit var inArticleImage: ImageView

    companion object{
        private var TAG = ArticleFragment::class.java.simpleName
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_article, container, false)
        val article : Article = args.articleArgs
        articleImage = view.findViewById(R.id.articleImage)
        articleHeadline = view.findViewById(R.id.articleTitle)
        articleBody = view.findViewById(R.id.articleBody)
        articleSection = view.findViewById(R.id.articleCategory)
        articlePub = view.findViewById(R.id.publishTime)
        articleTeaserBody = view.findViewById(R.id.articleTeaser)
        articleBodySub = view.findViewById(R.id.articleBodySub)
        inArticleImage = view.findViewById(R.id.inArticleImage)


        val xmlParser = XmlParser()
//        xmlParser.parser(article.properties[9].values[0])
//        val articleMap : LinkedHashMap<String, Any> = xmlParser.parse(StringReader(article.properties[10].values[0]))
        val articleMap : LinkedHashMap<String, String> = xmlParser.parse(StringReader(article.properties[10].values[0]))
        Log.d(TAG, "onCreateView: $articleMap")
        val imgUuid = articleMap["Image0"]
        val imgUrl = "${Constants.IMAGE_API}${imgUuid}${Constants.IMAGE_API_PROPERTIES}"
        Picasso.get().load(imgUrl).into(articleImage)
        articleHeadline.text = article.properties[4].values[0].trim()
        articleSection.text = article.properties[1].values[0].trim()
        articlePub.text = DateFormatter().format(article.properties[11].values[0].trim())
        articleTeaserBody.text = article.properties[2].values[0].trim()

        val articleBuilder = ArticleBuilder()
        articleBuilder.build(articleMap, articleBody)
        Log.d(TAG, "onCreateView: ${Constants.DATE_RANGE}")




//        when {
//            youTubeCount != "0" -> {
//
//            }
//            mediaCount != "0" -> {
//
//            }
//             imageCount != "0" -> {
//                val articleImageArray:ArrayList<String> = articleMap["Image"] as ArrayList<String>
//                val imgUrl = Constants.IMAGE_API+ articleImageArray[0]+ Constants.IMAGE_API_PROPERTIES
//                inArticleImage.visibility = View.VISIBLE
//                Picasso.get().load(imgUrl).into(inArticleImage)
//
//            }
//            linkCount != "0"->{
//
//            }
//            imageCount == "0" && youTubeCount == "0" && mediaCount == "0" && linkCount == "0"->{
//                val textArrayList = articleMap["ArticleBody"] as ArrayList<String>
//                var text = ""
//                textArrayList.forEach{
//                    text += it
//                }
//
//                articleBody.text = text
//            }
//
//        }


        return view
    }
    /**
     *  Add Map that returns a counter (Checks image and video placement), Article Body(ArrayList), in-article image,
     *  in-article videos
     *  Add two textviews and an image view, keep image view invisible till image is found.
     *
     */

    private fun initVideoPlayer(videoUri: String, view: View){
        val player = SimpleExoPlayer.Builder(view?.context).build()
        val mediaItem: MediaItem = MediaItem.fromUri(videoUri)
        player.setMediaItem(mediaItem)
        player.prepare()
        player.play()
    }

    private fun textViewDynamic(view: View, stringText: String){
        val parentLayout = view.findViewById(R.id.articleFragItem) as ConstraintLayout
        val set = ConstraintSet()
        val textView = TextView(view?.context)
        // set view id, else getId() returns -1
        // set view id, else getId() returns -1
        textView.id = View.generateViewId()
        parentLayout.addView(textView, 5)

        set.clone(parentLayout)
        // connect start and end point of views, in this case top of child to top of parent.
        // connect start and end point of views, in this case top of child to top of parent.
        set.connect(textView.id, ConstraintSet.TOP, R.id.articleBody, ConstraintSet.BOTTOM, 60)
        // ... similarly add other constraints
        // ... similarly add other constraints
        set.applyTo(parentLayout)
        textView.text = stringText
    }

}



