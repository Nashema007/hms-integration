package za.sargamatha.iol.views.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import za.sargamatha.iol.R


class MyNewsFragment : Fragment() {

    private lateinit var plusSymbol: ImageView
    private lateinit var gearSymbol: ImageView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
       val view = inflater.inflate(R.layout.fragment_my_news_empty, container, false)

        plusSymbol = view.findViewById(R.id.plus_symbol)
        gearSymbol = view.findViewById(R.id.gear_symbol)


        plusSymbol.setOnClickListener {
            val action = MyNewsFragmentDirections.actionMyNewsFragmentToAddSectionsFragment()
            findNavController().navigate(action)
        }

        gearSymbol.setOnClickListener {
            val action = MyNewsFragmentDirections.actionMyNewsFragmentToEditChoicesFragment()
            findNavController().navigate(action)
        }


        return view
    }


}