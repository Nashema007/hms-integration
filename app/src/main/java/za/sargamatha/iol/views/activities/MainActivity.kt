package za.sargamatha.iol.views.activities

import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.navigation.Navigation
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import za.sargamatha.iol.R

private lateinit var bottomNav: BottomNavigationView


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val window: Window = window

        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        // finally change the color
        window.statusBarColor = ContextCompat.getColor(this ,R.color.black);

        val navController = Navigation.findNavController(this,R.id.nav_host_fragment_container)

        bottomNav = findViewById(R.id.bottom_nav)

        bottomNav.setupWithNavController(navController)
        navController.addOnDestinationChangedListener { controller, destination, arguments -> 
            if (destination.id == R.id.featuredFragment || destination.id == R.id.latestNewsFragment || destination.id == R.id.myNewsFragment){
                bottomNav.visibility = View.VISIBLE
            }
            else {
                bottomNav.visibility = View.INVISIBLE
            }
        }


    }


}