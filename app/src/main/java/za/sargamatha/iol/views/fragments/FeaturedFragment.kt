package za.sargamatha.iol.views.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import za.sargamatha.iol.R
import za.sargamatha.iol.adapter.ArticleAdapter
import za.sargamatha.iol.models.Article
import za.sargamatha.iol.utils.FeaturedarticleParser
import za.sargamatha.iol.viewmodel.ArticleViewModel
import java.io.StringReader


class FeaturedFragment : Fragment() {

    private lateinit var articleRecyclerView: RecyclerView
    private lateinit var articleAdapter: ArticleAdapter
    private lateinit var infoBtn: ImageView
    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var articleViewModel: ArticleViewModel

    companion object {
        private var TAG = FeaturedFragment::class.java.simpleName
        private const val CLASS_FROM = "Featured"
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_featured, container, false)
        articleRecyclerView = view.findViewById(R.id.featured_recyclerview)
        infoBtn = view.findViewById(R.id.info_tab)
        articleViewModel = ViewModelProvider(this).get(ArticleViewModel::class.java)
        swipeRefresh = view.findViewById(R.id.swipeRefresher)

        infoBtn.setOnClickListener {
            val action = FeaturedFragmentDirections.actionFeaturedFragmentToInfoFragment()
            findNavController().navigate(action)
        }

        articleInit()

        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = false
            articleAdapter.clear()
            articleInit()
        }

        return view
    }

    private fun initAdapter() {
        articleAdapter = ArticleAdapter()
        articleRecyclerView.layoutManager = LinearLayoutManager(view?.context)
        articleRecyclerView.adapter = articleAdapter
    }

    private  fun articleInit(){
        articleViewModel.fetchFeaturedList()
        articleViewModel.articleFeaturedList.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                val articleUUid = FeaturedarticleParser().parse(StringReader(it))
                val arrayListArticle = ArrayList<Article>()
                for (element in articleUUid) {
                    articleViewModel.fetchArticle(element)
                    articleViewModel.articleListLiveData.observe(
                        viewLifecycleOwner,
                        Observer { articleItem ->
                            if (articleItem != null) {
                                arrayListArticle.add(articleItem)
                                initAdapter()
                                articleAdapter.setData(arrayListArticle, CLASS_FROM)


                            }
                        })
                }
            }

        })


    }

}