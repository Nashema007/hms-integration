package za.sargamatha.iol.views.fragments

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.huawei.agconnect.config.AGConnectServicesConfig
import com.huawei.hms.aaid.HmsInstanceId
import com.huawei.hms.common.ApiException
import com.huawei.hms.push.HmsMessaging
import za.sargamatha.iol.R
import za.sargamatha.iol.adapter.EditSectionAdapter


class EditChoicesFragment : Fragment() {

    private lateinit var toolbar: Toolbar
    private lateinit var recyclerView: RecyclerView
    private var receiver: MyReceiver? = null
    private lateinit var adapter:EditSectionAdapter

    companion object {
        private val TAG = EditChoicesFragment::class.java.simpleName
        private const val GET_AAID = 1
        private const val DELETE_AAID = 2
        private const val CODELABS_ACTION: String = "com.huawei.codelabpush.action"

    }

    var handler: Handler? = object : Handler(Looper.getMainLooper()) {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                GET_AAID -> Log.d(TAG, "handleMessage: Get AAID")
                DELETE_AAID -> Log.d(TAG, "handleMessage: DELETE AAID")
                else -> {
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_edit_choices, container, false)
        receiver = MyReceiver()
        toolbar = view.findViewById(R.id.editChoicesToolbar)
        recyclerView = view.findViewById(R.id.editChoiceRecyclerView)
        var testBtn = view.findViewById<Button>(R.id.testPush)

        testBtn.setOnClickListener {
            getToken()
        }

        activity?.setActionBar(toolbar)
        activity?.actionBar?.setDisplayHomeAsUpEnabled(true)
        activity?.actionBar?.setDisplayShowHomeEnabled(true)
        toolbar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }
        val intentFilter = IntentFilter()
        intentFilter.addAction(CODELABS_ACTION)
        activity?.registerReceiver(receiver, intentFilter)


        initAdapter()


        return view


    }

    private fun initAdapter(){
        adapter = EditSectionAdapter()
        recyclerView.layoutManager = LinearLayoutManager(view?.context)
        recyclerView.adapter = adapter
        val listItems = ArrayList<String>()
        listItems.add("SPORT")
        listItems.add("TECH")
        listItems.add("BUSINESS REPORT")
        listItems.add("ENTERTAINMENT")
        listItems.add("LIFESTYLE")
        listItems.add("MOTORING")
        listItems.add("TRAVEL")
        listItems.add("PERSONAL FINANCE")
        adapter.setData(listItems)
    }


    /**
     * getToken(String appId, String scope), This method is used to obtain a token required for accessing HUAWEI Push Kit.
     * If there is no local AAID, this method will automatically generate an AAID when it is called because the Huawei Push server needs to generate a token based on the AAID.
     * This method is a synchronous method, and you cannot call it in the main thread. Otherwise, the main thread may be blocked.
     */
    private fun getToken() {
        showLog("getToken:begin")
        object : Thread() {
            override fun run() {
                try {
                    // read from agconnect-services.json
                    val appId = AGConnectServicesConfig.fromContext(view?.context).getString("client/app_id")
                    val token = HmsInstanceId.getInstance(view?.context).getToken(appId, "HCM")
                    Log.i(TAG, "get token:$token")
                    if (!TextUtils.isEmpty(token)) {
                        sendRegTokenToServer(token)
                    }
                    showLog("get token:$token")
                } catch (e: ApiException) {
                    Log.e(TAG, "get token failed, $e")
                    showLog("get token failed, $e")
                }
            }
        }.start()
    }

    /**
     * void deleteToken(String appId, String scope) throws ApiException
     * This method is used to obtain a token. After a token is deleted, the corresponding AAID will not be deleted.
     * This method is a synchronous method. Do not call it in the main thread. Otherwise, the main thread may be blocked.
     */
    private fun deleteToken() {
        showLog("deleteToken:begin")
        object : Thread() {
            override fun run() {
                try {
                    // read from agconnect-services.json
                    val appId = AGConnectServicesConfig.fromContext(view?.context).getString("client/app_id")
                    HmsInstanceId.getInstance(view?.context).deleteToken(appId, "HCM")
                    Log.i(TAG, "deleteToken success.")
                    showLog("deleteToken success")
                } catch (e: ApiException) {
                    Log.e(TAG, "deleteToken failed.$e")
                    showLog("deleteToken failed.$e")
                }
            }
        }.start()
    }

    private fun sendRegTokenToServer(token: String?) {
        Log.i(TAG, "sending token to server. token:$token")
    }


    /**
     * Set up enable or disable the display of notification messages.
     * @param enable enabled or not
     */
    private fun setReceiveNotifyMsg(enable: Boolean) {
        showLog("Control the display of notification messages:begin")
        if (enable) {
            HmsMessaging.getInstance(view?.context).turnOnPush().addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    showLog("turnOnPush Complete")
                } else {
                    showLog("turnOnPush failed: cause=" + task.exception.message)
                }
            }
        } else {
            HmsMessaging.getInstance(view?.context).turnOffPush().addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    showLog("turnOffPush Complete")
                } else {
                    showLog("turnOffPush  failed: cause =" + task.exception.message)
                }
            }
        }
    }

    /**
     * getAAID(), This method is used to obtain an AAID in asynchronous mode. You need to add a listener to listen to the operation result.
     * deleteAAID(), delete a local AAID and its generation timestamp.
     * @param isGet getAAID or deleteAAID
     */
    private fun setAAID(isGet: Boolean) {
        if (isGet) {
            val idResult = HmsInstanceId.getInstance(view?.context).aaid
            idResult.addOnSuccessListener { aaidResult ->
                val aaId = aaidResult.id
                Log.i(TAG, "getAAID success:$aaId")
                showLog("getAAID success:$aaId")
                handler?.sendEmptyMessage(DELETE_AAID)
            }.addOnFailureListener { e ->
                Log.e(TAG, "getAAID failed:$e")
                showLog("getAAID failed.$e")
            }
        } else {
            object : Thread() {
                override fun run() {
                    try {
                        HmsInstanceId.getInstance(view?.context).deleteAAID()
                        showLog("delete aaid and its generation timestamp success.")
                        handler?.sendEmptyMessage(GET_AAID)
                    } catch (e: Exception) {
                        Log.e(TAG, "deleteAAID failed. $e")
                        showLog("deleteAAID failed.$e")
                    }
                }
            }.start()
        }
    }

    /**
     * to subscribe to topics in asynchronous mode.
     */
    private fun addTopic(topic: String) {
        try {
            HmsMessaging.getInstance(view?.context)
                .subscribe(topic)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Log.i(TAG, "subscribe Complete")
                        showLog("subscribe Complete")
                    } else {
                        showLog("subscribe failed: ret=" + task.exception.message)
                    }
                }
        } catch (e: Exception) {
            showLog("subscribe failed: exception=" + e.message)
        }
    }

    /**
     * to unsubscribe to topics in asynchronous mode.
     */
    private fun deleteTopic(topic: String) {
        try {
            HmsMessaging.getInstance(view?.context)
                .unsubscribe(topic)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Log.d(TAG, "onConfirmClick: unsubscribe Complete")
                    } else {
                        Log.d(
                            TAG,
                            "onConfirmClick: unsubscribe failed: ret=" + task.exception.message
                        )
                    }
                }
        } catch (e: Exception) {
            Log.d(TAG, "onConfirmClick: unsubscribe failed: exception=" + e.message)
        }
    }

    /**
     * MyReceiver
     */
    inner class MyReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val bundle = intent?.extras
            if (bundle?.getString("msg") != null) {
                val content = bundle.getString("msg")
                Log.d(TAG, "onReceive: $content")
            }
        }
    }

    private fun showLog(msg: String){

        Log.d(TAG, "Show Log: $msg")
    }
    override fun onDestroyView() {
        super.onDestroyView()
        activity?.unregisterReceiver(receiver)
    }

}