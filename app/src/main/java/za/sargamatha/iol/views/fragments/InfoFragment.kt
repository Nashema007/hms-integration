package za.sargamatha.iol.views.fragments

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toolbar
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import za.sargamatha.iol.R
import za.sargamatha.iol.utils.Constants



class InfoFragment : Fragment() {

    private lateinit var contactCardView: CardView
    private lateinit var toolbar: Toolbar

    companion object{
        private val TAG = InfoFragment::class.java.simpleName
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_info, container, false)


        contactCardView = view.findViewById(R.id.contactCardView)
        toolbar = view.findViewById(R.id.infoToolBar)

        activity?.setActionBar(toolbar)
        activity?.actionBar?.setDisplayHomeAsUpEnabled(true)
        activity?.actionBar?.setDisplayShowHomeEnabled(true)
        toolbar.setNavigationOnClickListener{
                findNavController().navigateUp()
        }
        try {
            val pInfo = requireContext().packageManager.getPackageInfo(
                requireContext().packageName, 0
            )
            val versionName = pInfo.versionName
            val versionCode = pInfo.versionCode
            Log.d(TAG, "onCreateView: $versionName $versionCode")
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        contactCardView.setOnClickListener {
            val intent = Intent(Intent.ACTION_SEND)
            intent.putExtra(Intent.EXTRA_EMAIL, Constants.EMAIL_EMAIL)
            intent.putExtra(Intent.EXTRA_TEXT, Constants.EMAIL_MESSAGE)
            intent.type = "message/rfc822"
            startActivity(Intent.createChooser(intent, "Select email"))
        }



        return view
    }


}