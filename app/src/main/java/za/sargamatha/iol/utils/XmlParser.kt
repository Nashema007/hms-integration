package za.sargamatha.iol.utils

import android.util.Xml
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserException
import java.io.IOException
import java.io.StringReader

// We don't use namespaces
private const val ns: String = "http://www.infomaker.se/idf/1.0"
private const val YOUTUBE_TYPE = "x-im/youtube"
private const val IMAGE_TYPE = "x-im/image"
private const val MEDIA_EMBED = "x-im/htmlembed"

class XmlParser {

companion object{
    private var TAG = XmlParser::class.java.simpleName

}
    private val articleBody = ArrayList<String>()
    private val articleYouTubeVideo = ArrayList<String>()
    private val articleImage = ArrayList<String>()
    private val articleMedia = ArrayList<String>()
    private val articleLinks = ArrayList<String>()
    private val articleLinkText = ArrayList<String>()
    private val articleMap = LinkedHashMap<String, Any>()
    private val articleTest = LinkedHashMap<String, String>()

    private var checkBody = ""
    private var youTubePara = ArrayList<Int>()
    private var imagePara = ArrayList<Int>()
    private var linkPara = ArrayList<Int>()
    private var mediaEmbedPara = ArrayList<Int>()
    private var paragraphCount = 0
    private var checkLinkText = ""

    @Throws(XmlPullParserException::class, IOException::class)
fun parse(inputStream: StringReader): LinkedHashMap<String, String>{
    val parser: XmlPullParser = Xml.newPullParser()
    parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false)
    parser.setInput(inputStream)
    var eventType = parser.eventType
    while (eventType != XmlPullParser.END_DOCUMENT) {
        when (eventType) {
            XmlPullParser.START_DOCUMENT -> {
                println("Start document");
            }
            XmlPullParser.START_TAG -> {
                setArticleBody(parser)
                linkTags(parser)
                setLinkText(parser)
                imgType(parser)
                youTube(parser)
                mediaEmbed(parser)
                println("Start tag " + parser.name)
            }
            XmlPullParser.END_TAG -> {
                if (parser.name == "element") {
                    println("End tag " + parser.name)
                }
                checkBody = ""
            }
            XmlPullParser.TEXT -> {
                textTag(parser)
                println("Text " + parser.text)
            }

        }
        eventType = parser.next()
    }
       // initMap()
        //return articleMap
        return articleTest
}

    private fun initMap() {
        articleMap["YoutubeCount"] = youTubePara
        articleMap["Youtube"] = articleYouTubeVideo
        articleMap["ArticleBody"] = articleBody
        articleMap["MediaEmbed"] = articleMedia
        articleMap["MediaEmbedCount"] = mediaEmbedPara
        articleMap["Image"] = articleImage
        articleMap["ImageCount"] = imagePara
        articleMap["ArticleLinks"] = articleLinks
        articleMap["ArticleLinkCount"] = linkPara
        articleMap["ArticleLinkText"] = articleLinkText
    }

    private fun textTag(parser: XmlPullParser) {
        when {
            checkBody == "body" && checkBody != "" -> {
                println("Text " + parser.text)
                articleBody.add(parser.text)
                articleTest["Body$paragraphCount"] = parser.text
                paragraphCount += 1
                if (checkLinkText == "check" && checkLinkText != "") {
                    articleLinkText.add(parser.text)

                    checkLinkText = ""
                }
            }
            checkBody == MEDIA_EMBED -> {
                // Add function to parse media embed
                articleMedia.add(parser.text)
                //articleTest["MediaLink$paragraphCount"] = parser.text
                mediaEmbedPara.add(paragraphCount)
               // checkBody = ""
            }
        }
    }

    private fun linkTags(parser: XmlPullParser){
        if (parser.name == "a"){
            articleLinks.add(parser.getAttributeValue(null, "href"))
            linkPara.add(paragraphCount)
            //articleTest["LinkHref$paragraphCount"] = parser.getAttributeValue(null, "href")
        }
    }
    private fun setLinkText(parser: XmlPullParser){
        if (parser.name == "strong"){
            checkLinkText = "check"
        }
    }
    private fun setArticleBody(parser: XmlPullParser){
        if (parser.name == "element") {
            val attrTypeBody = parser.getAttributeValue(null, "type")
            if (attrTypeBody == "body") {
                checkBody = "body"

            }
        }
    }
    private fun imgType(parser: XmlPullParser){
        if (parser.name == "link") {
            if(parser.getAttributeValue(null, "type") == IMAGE_TYPE){
                articleImage.add(parser.getAttributeValue(null, "uuid"))
                articleTest["Image$paragraphCount"]= parser.getAttributeValue(null, "uuid")
                imagePara.add(paragraphCount)
            }
        }
    }
    private fun youTube(parser: XmlPullParser) {
        if (parser.name == "object") {
            if (parser.getAttributeValue(null, "type") == YOUTUBE_TYPE) {
                articleYouTubeVideo.add(parser.getAttributeValue(null, "uri"))
                //articleTest["Youtube$paragraphCount"] = parser.getAttributeValue(null, "uri")
                youTubePara.add(paragraphCount)
            }
        }
    }

        private fun mediaEmbed(parser: XmlPullParser){
            if (parser.name == "text") {
                checkBody = MEDIA_EMBED
                }
        }



//    private fun getObject(parser: XmlPullParser){
//        if (parser.name == "object") {
//            when (parser.getAttributeValue(null, "type")) {
//                YOUTUBE_TYPE -> {
//                    articleYouTubeVideo.add(parser.getAttributeValue(null, "uri"))
//                    youTubePara = paragraphCount
//                }
//                MEDIA_EMBED -> {
//                    if (parser.name == "text") {
//                        checkBody = MEDIA_EMBED
//                    }
//                }
//                IMAGE_TYPE -> {
//                   imgType(parser)
//                }
//            }
//        }
//    }


}