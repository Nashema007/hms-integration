package za.sargamatha.iol.utils

import android.util.Xml
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserException
import java.io.IOException
import java.io.StringReader

class FeaturedarticleParser {

    private val articleUuid = ArrayList<String>()

    @Throws(XmlPullParserException::class, IOException::class)
    fun parse(inputStream: StringReader): ArrayList<String>{
        val parser: XmlPullParser = Xml.newPullParser()
        parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false)
        parser.setInput(inputStream)
        var eventType = parser.eventType
        while (eventType != XmlPullParser.END_DOCUMENT) {
            when (eventType) {
                XmlPullParser.START_DOCUMENT -> {
                    println("Start document");
                }
                XmlPullParser.START_TAG -> {
                    setArticleList(parser)
                    println("Start tag " + parser.name)
                }
                XmlPullParser.END_TAG -> {
                    println("End tag " + parser.name)

                }
            }
            eventType = parser.next()
        }

        return articleUuid
    }

    private fun setArticleList(parser: XmlPullParser){
        if (parser.name == "item"){
            articleUuid.add(parser.getAttributeValue(null, "uuid"))

        }
    }
}