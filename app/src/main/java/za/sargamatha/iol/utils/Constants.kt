package za.sargamatha.iol.utils

import java.text.SimpleDateFormat
import java.util.*


object Constants {
    const val BASE_URL = "https://xlibris.public.prod.oc.inl.infomaker.io:8443/opencontent/"
    const val IMAGE_API = "https://imengine.public.prod.inl.infomaker.io/?uuid="
    const val IMAGE_API_PROPERTIES = "&function=np_crop&type=preview&source=false&q=75&width=960&height=540&x=1&y=1&z=0.5"
    const val GET_UUID_PROPERTIES = "uuid,Pubdate,Published,Status,updated,Authors[Name,Email]"
    const val GET_ARTICLE_PROPERTIES = "BodyRaw, TeaserHeadline, TeaserBody, Section, ConceptAuthorNames,Pubdate,Headline,updated,ObjectUpdated,ImageUuids,TeaserImageUuids,TeaserRaw"
    const val CHANNEL = "Channels:IOL"
    const val FEATURED_LIST_UUID ="cb183cd8-c2cc-447f-a0e4-299022808405"
    const val EMAIL_EMAIL = "appsuport@inl.co.za"
    const val EMAIL_MESSAGE = "appsuport@inl.co.za"
    const val AUTH_USERNAME = "devadmin"
    const val AUTH_PASSWORD = "wdQxm4evNYmK"
    const val LIMIT = "50"
    const val CONTENT_TYPE ="Article"
    const val SORT_INDEX = "Pubdate"
    const val SORT_VALUE = "false"
    const val TECH_UUID=""
    const val SPORT_UUID=""
    const val BUSINESS_REPORT = ""
    const val ENTERTAINMENT =""
    const val LIFESTYLE = ""
    const val MOTORING =""
    const val TRAVEL = ""
    const val PERSONAL_FINANCE = ""
    var DATE_RANGE = "ObjectUpdated:${getDate()}"
    /**
     *
     * TeaserBody:	Defender Simon Kjaer scored in stoppage time as AC Milan earned a 1-1 draw against Manchester United in a Europa League last-16 first leg clash at Old Trafford.
     * TeaserHeadline:	Milan strike late to draw with Man United
     * TeaserImageCrop:	im://crop/0/0.014925373134328358/1/0.8414179104477612
     * TeaserImageCrops:	16:9=im://crop/0/0.014925373134328358/1/0.8414179104477612,1:1=im://crop/0.165/0/0.67/1.001865671641791
     * TeaserImageHeight:	1324
     * TeaserImageUri:	im://image/geIIm8jg0LH0J_HHMpVcNXMPd6s.jpg
     * TeaserImageUuids:	12558278-ef18-5578-a6e9-0f5d5b509d86
     * TeaserImageWidth:	1974
     * TeaserRaw:	Milan strike late to draw with Man UnitedDefender Simon Kjaer scored in stoppage time as AC Milan earned a 1-1 draw against Manchester United in a Europa League last-16 first leg clash at Old Trafford.Manchester United's Amad Diallo celebrates after scoring their first goal in their Europa League round of 16 first leg clash against AC Milan at Old Trafford on Thursday. Photo: Phil Noble/Reuters19741324
     */

    /**
     *
     * search?
     * contenttype=Article&
     * limit=10&
     * sort.indexfield=Pubdate&
     * sort.Pubdate.ascending=false&
     * q=ArticleMetaSectionUuid:5824419c-aac0-4324-8744-3c3617861ebc+AND+Channels:IOL&
     * properties=uuid
     */

    private fun getDate(): String{
        val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US)
        formatter.timeZone = TimeZone.getTimeZone("GMT+02:00")
        val date: Date = Calendar.getInstance().time
        val today = formatter.format(date)
        val weekAgo = formatter.format(Date(date.year, date.month,( date.date -7), date.hours, date.seconds))
        return "[$weekAgo+TO+$today]"
    }



}