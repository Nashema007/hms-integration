package za.sargamatha.iol.utils

import java.text.SimpleDateFormat
import java.util.*

private const val DAY_TIME = "EEEE HH:mm"

class DateFormatter {

    fun format(time: String): String{
        val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US)
        formatter.timeZone = TimeZone.getTimeZone("GMT+02:00")
        val pubDate:Date? = formatter.parse(time)
        val formatPub = SimpleDateFormat(DAY_TIME, Locale.US)
        formatPub.timeZone = TimeZone.getTimeZone("GMT+04:00")
        return formatPub.format(pubDate)
    }


}