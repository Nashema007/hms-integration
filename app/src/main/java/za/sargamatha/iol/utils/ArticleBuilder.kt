package za.sargamatha.iol.utils

import android.util.Log
import android.widget.TextView



class ArticleBuilder {

    companion object{
        private var TAG = ArticleBuilder::class.java.simpleName
    }


    fun build(articleMap: LinkedHashMap<String, String>, textView: TextView) {
        articleMap.remove("Image0")
        //articleMap.remove("MediaLink0")
        val body = articleMap.values
        Log.d(TAG, "build: ${Regex.fromLiteral("1edb324f-02b7-5765-a9e6-bbce470a8a3b")}")
        var bodyString = ""
        body.forEach{

            bodyString += "$it\n\n"
        }
        textView.text = bodyString

    }

    private fun createLink(text:String, link: String): String {
        return  "<a href='$link'>$text</a>"

    }

  //  private
}