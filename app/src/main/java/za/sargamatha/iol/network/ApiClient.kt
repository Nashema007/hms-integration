package za.sargamatha.iol.network

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import za.sargamatha.iol.utils.Constants
import java.util.concurrent.TimeUnit

interface ApiClient {


    companion object{
        private var retrofit: Retrofit? = null
        fun getApiClient(): Retrofit{
        val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            val gson = GsonBuilder()
                .setLenient()
                .create()
            val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(BasicAuthInterceptor(Constants.AUTH_USERNAME,Constants.AUTH_PASSWORD))
                .addInterceptor(httpLoggingInterceptor)
                .readTimeout(100, TimeUnit.SECONDS)
                .connectTimeout(100 ,TimeUnit.SECONDS)
                .build()
            if (retrofit == null){
                retrofit = Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .client(okHttpClient)
                    .addConverterFactory(ScalarsConverterFactory.create()) // to parse string requests
                    .addConverterFactory(GsonConverterFactory.create(gson)) // to parse json requests
                    .build()
            }


            return retrofit!!
        }
    }
}