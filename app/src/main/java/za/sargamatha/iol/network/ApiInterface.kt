package za.sargamatha.iol.network

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import za.sargamatha.iol.models.Article
import za.sargamatha.iol.models.ArticleConcept
import za.sargamatha.iol.models.SectionConcept

interface ApiInterface {

    @GET("search")
    fun fetchAllArticleUuids(
        @Query("q", encoded = true) dateRange: String,
        @Query("q",encoded = true) channel:String,
        @Query("contenttype",encoded = true) contentType: String,
        @Query("sort.indexfield",encoded = true) sortIndexField: String,
        @Query("sort.Pubdate.ascending",encoded = true) sortOrder: String,
        @Query("properties",encoded = true) properties: String,
        @Query("limit",encoded = true) limit: String): Call<ArticleConcept>


    @GET("objects/{uuid}/properties")
    fun fetchAllArticles(
        @Path("uuid") uuid: String,
        @Query("properties", encoded = true) properties: String):Call<Article>

    @GET("objects/{uuid}")
    fun fetchFeaturedList(@Path("uuid",encoded = true) uuid: String):Call<String>

    @GET("search")
    fun fetchAllArticlesSection(
        @Query("contenttype", encoded = true) contentType: String,
        @Query("limit",encoded = true) limit: String,
        @Query("sort.indexfield", encoded = true) sortIndexField: String,
        @Query("sort.Pubdate.ascending", encoded = true) sortOrder: String,
        @Query("q", encoded = true) articleMeta: String,
        @Query("properties", encoded = true)properties: String,
    ): Call<SectionConcept>
}