package za.sargamatha.iol.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import za.sargamatha.iol.models.Article
import za.sargamatha.iol.models.ArticleConcept
import za.sargamatha.iol.repository.ArticleConceptRepository

class ArticleViewModel: ViewModel() {

    private var articleConceptRepository: ArticleConceptRepository?= null
    var articleConceptListLiveData: LiveData<ArticleConcept> = MutableLiveData()
    var articleListLiveData: LiveData<Article> = MutableLiveData()
    var articleFeaturedList: LiveData<String> = MutableLiveData()
    init {
        articleConceptRepository = ArticleConceptRepository()
    }

    fun fetchArticle(list: String){
        articleListLiveData = articleConceptRepository?.fetchArticles(list)!!
    }

    fun fetchArticleConceptUuids(){
        articleConceptListLiveData = articleConceptRepository?.fetchArticleUuuids()!!
    }

    fun fetchFeaturedList(){
        articleFeaturedList = articleConceptRepository?.fetchFeaturedList()!!
    }

}

