package za.sargamatha.iol.models

import com.google.gson.annotations.SerializedName

data class ArticleConcept (
    @SerializedName("hits")
    val hits: Hits
    )

data class Hits(
    @SerializedName("totalHits")
    val totalHits: String = " ",
    @SerializedName("hits")
    val hits: List<ArticleHits>
)

data class ArticleHits(
    @SerializedName("id")
    val id : String =" ",
    @SerializedName("versions")
    val versions: List<Versions>

)
data class Versions(
    val id:String = " ")
//"id": "45d46cb8-2dae-4e06-a33b-094d78d2f6d8",
//    "versions": [
//    {
//        "id": 3,
//        "properties": {
//        "Status": [
//        "usable"
//        ],
//        "Pubdate": [
//        "2021-03-09T08:00:00Z"
//        ],
//        "Authors": [
//        {
//            "Name": [
//            "The Washington Post"
//            ]
//        }
//        ],
//        "uuid": [
//        "45d46cb8-2dae-4e06-a33b-094d78d2f6d8"
//        ],
//        "updated": [
//        "2021-03-09T08:00:46Z"
//        ]
//    }
//    }
//    ],
//    "noVersions": 1
//}


//
//@SerializedName("uuid")
//val uuid: String = " ",
//@SerializedName("Status")
//val status: String = " ",
//@SerializedName("Pubdate")
//val pubDate: String = " ",
//@SerializedName("Authors")
//val authors: Author,
//@SerializedName("updated")
//val updated: String = " "