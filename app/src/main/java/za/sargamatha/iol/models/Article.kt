package za.sargamatha.iol.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Article(
    @SerializedName("contentType")
    val contentType : String = " ",
    @SerializedName("editable")
    val editable: String = " ",
    @SerializedName("status")
    val status: String = "",
    @SerializedName("properties")
    val properties: List<ArticleProperties>,


) : Parcelable

@Parcelize
data class ArticleProperties(
    @SerializedName("name")
    val name : String,
    @SerializedName("values")
    val values: List<String>
):Parcelable
